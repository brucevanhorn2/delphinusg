<?php
    include_once('php/auth_header.php');
?>
<html>
<head>
<title>Delphinvs</title>
<link rel="stylesheet" type="text/css" href="third-party/ext-4.0.7-gpl/resources/css/ext-all.css" />
<link rel="stylesheet" href="third-party/code-mirror-2.23/lib/codemirror.css" />
<link rel="stylesheet" href="css/editor.css" />

<script type="text/javascript" src="third-party/ext-4.0.7-gpl/bootstrap.js"></script>

<script src="third-party/code-mirror-2.23/lib/codemirror.js"></script>
<script type="text/javascript" src="third-party/code-mirror-2.23/mode/mysql/mysql.js"></script>
<script type="text/javascript">
    Ext.require(['*']);

    var tabStrip;
    var fileTree;
    var tabCounter = 0;
    
    var winNewDatabase={};
    var winNewTable={};
    var winSecurity = {};
    
    Ext.onReady(function() {
        
            Ext.state.Manager.setProvider(Ext.create('Ext.state.CookieProvider'));
            
            var store = Ext.create('Ext.data.TreeStore', {
            proxy: {
                type: 'ajax',
                url: 'php/list_databases.php'
            },
            root: {
                text: 'Databases',
                id: 'src',
                expanded: true
            },
            folderSort: false
        });
            
    tabStrip = Ext.create('Ext.tab.Panel', {
        region: 'center', 
        deferredRender: false,
        activeTab: 0,  
        items: [{
            contentEl: 'center1',
            title: 'Welcome',
            closable: true,
            autoScroll: true
        }]
    });
    
    scriptMenu = Ext.create('Ext.menu.Menu',{
        items:[
            {text:'Select'}
            ,{text:'Insert'}
            ,{text:'Update'}
            ,{text:'Create'}
            ,{text:'Drop'}
        ]    
    });
    
    fileMenu = Ext.create('Ext.menu.Menu', {
        items: [{
            text: 'New'
            ,iconCls:'newProject'
            ,menu:{
                items:[
                    {
                        text:'Table'
                        ,iconCls:'table'    
                    }
                    ,{
                        text:'View'
                        ,iconCls:'view'
                    }
                    ,{
                        text:'Stored Procedure'
                        ,iconCls:'storedProcedure'
                    }
                    ,{
                        text:'Function'
                        ,iconCls:'function'
                    }
                ]
            }
        },{
            text: 'Open'
            ,iconCls:'openProject'
        },
        {
            text: 'Edit Definition'
            ,iconCls:'editTable'
        }
        ,{
            text: 'Delete'
            ,iconCls:'delete'
        },{
            text: 'Script Item As...'
            ,iconCls:'scriptWindow'
            ,menu:{
                items:[
                      {
                          text:'Select'
                          ,iconCls:'select'
                      }
                      ,{
                          text:'Insert'
                          ,iconCls:'insertRow'
                      }
                      ,{
                          text:'Update'
                          ,iconCls:'editTable'
                      }
                      ,{
                          text:'Create'
                          ,iconCls:'newTable'
                      }
                      ,{
                          text:'Drop'
                          ,iconCls:'deleteTable'
                      }
                    ]
            }
        }]    
    });
    
    fileTree = Ext.create('Ext.tree.Panel', {
        store: store,
        height: 300,
       useArrows: true,
       dblClickExpand:false,
       enableDD:false,
       rootVisible:false
    });
        
        fileTree.on("itemcontextmenu", function(node, record,item, index, e){
                fileMenu.showAt(e.getXY());
                e.stopEvent();
        });
        
        fileTree.on("itemclick", function(node, record){
                //alert('click' + record.data.id);
                    var split = record.data.id.split(":");
                    var type = split[0];
                    var tableName = split[2];
                    var isLeaf = record.data.leaf;
                    if(type=="FIELD" && !isLeaf) {
                                tabStrip.add({
                                title:"Table: " + tableName
                                ,closable: true
                                ,autoScroll: true
                                ,tbar:[
                                    {iconCls: 'refresh', text:'Refresh'}
                                    ,{iconCls: 'save', text:'Save'}
                                    ,'->'
                                    ,{iconCls:'help'}
                                ]    
                                });
                                var items = tabStrip.items.items;
                                var totalTabs = items.length;
                                tabStrip.setActiveTab(totalTabs-1);
                    }
                    
                
                }
                
            );
        
        var viewport = Ext.create('Ext.Viewport', {
            id: 'border-example',
            layout: 'border',
            items: [
            
            {
                region:'north',
                title:'Delphinvs - The Best Web UI for MySQL'
                ,tbar:[
                    {iconCls: 'newDatabase'
                     ,handler:function(){
                            winNewDatabase.show();
                        }
                     }
                    ,{iconCls: 'deleteDatabase'}
                    ,{iconCls: 'editDatabase'}
                    ,'-'
                    ,{iconCls: 'newTable'
                       ,handler: function(){winNewTable.show();}
                       }
                    ,{iconCls: 'deleteTable'}
                    ,{iconCls: 'editTable'}
                    ,'-'
                    ,{iconCls: 'scriptWindow'
                       ,handler:function(){
                            tabCounter++;
                            var tabName = 'tab' + tabCounter;
                            var html = '<textarea id="editwindow"' + tabCounter + ' rows="2" cols="20">'
                            
                            tabStrip.add({
                                title:"Untitled Script - " + tabCounter
                                ,closable: true
                                ,autoScroll: true
                                ,tbar:[
                                    {iconCls: 'load', text:'Load'}
                                    ,{iconCls: 'save', text:'Save'}
                                    ,'-'
                                    ,{iconCls:'executeQuery'
                                        ,text:'Execute Query'}
                                    ,{iconCls:'explainQuery', text:'Explain Query'}
                                    ,'->'
                                    ,{iconCls:'help'}
                                ]
                                ,items:[
                                    {
                                        xtype:'textarea'
                                        ,name:tabName
                                        ,emptyText:tabName
                                        ,id:tabName
                                        ,listeners : {
                                              'render' : function(thisCmp) {
                                                    var myTextArea = document.getElementById(tabName);
                                                    var myCodeMirror = CodeMirror.fromTextArea(myTextArea, {mode:'mysql',lineNumbers:'true', autofocus:'true',});
                                                    var items = tabStrip.items.items;
                                                    var totalTabs = items.length;
                                                    if(totalTabs > 0){
                                                        tabStrip.setActiveTab(totalTabs-1);
                                                    }
                                            }
                                        }
                                    }
                                ]
                            })
                            
                        }
                    }
                    ,'-'
                    ,{iconCls: 'security'
                       ,handler:function(){
                            winSecurity.show(); 
                       }}
                ]
            }, {
                
                region: 'south',
                contentEl: 'south',
                split: true,
                height: 100,
                minSize: 100,
                maxSize: 200,
                collapsible: true,
                collapsed: true,
                title: 'Output',
                margins: '0 0 0 0'
            }, {
                region: 'west',
                stateId: 'navigation-panel',
                id: 'west-panel',
                title: 'Databases',
                iconCls: 'dataSources',
                split: true,
                width: 200,
                minWidth: 175,
                maxWidth: 400,
                collapsible: true,
                animCollapse: true,
                margins: '0 0 0 5',
                layout: 'fit',
                items: [{
                    contentEl: 'west',
                    items:[fileTree]
                }]
            }, tabStrip
        ]
        });
        
    });
    
    //windows
    //new database
    
    var dbTypes = [
        [1, 'Production']
       ,[2, 'Testing (Internal)']
       ,[3, 'Testing (Customer Facing)']
       ,[4, 'Development']
    ];
  
    
    var cboDBType = new Ext.form.ComboBox({
        store: new Ext.data.SimpleStore({
             id:0
            ,fields:
                [
                    'dbId',   
                    'dbText'
                ]
            ,data:dbTypes
        })
        ,valueField:'dbId'
        ,displayField:'dbText'
        ,mode:'local'
        ,fieldLabel:'Database Type'
    });
    
    newDatabaseForm = Ext.create('Ext.form.Panel', {
                frame: true,
                bodyPadding: 15,
                fieldDefaults: {
                    labelAlign: 'left',
                    labelWidth: 100,
                    anchor: '100%'}
                    ,items: [{
                        xtype: 'textfield',
                        name: 'dbname',
                        fieldLabel: 'Database Name'
                    },
                    cboDBType
                    ,{
                        xtype: 'textarea',
                        name: 'comments',
                        fieldLabel: 'Comments'
                    }
                    ]
                    ,fbar:[{
                        iconCls: 'close'
                        ,text:'Close'
                    },{
                        iconCls: 'save'
                        ,text: 'Save'}
                    ]
              
            });
    
    winNewDatabase = Ext.create('widget.window', {
                title: 'New Database',
                closable: true,
                closeAction: 'hide',
                width: 400,
                minWidth: 250,
                height: 256,
                bodyStyle: 'padding: 5px;',
                items: [newDatabaseForm]
            });
   
    //new table
    winNewTable = Ext.create('widget.window', {
                title: 'New Table',
                closable: true,
                closeAction: 'hide',
                width: 600,
                minWidth: 350,
                height: 350,
                layout: 'fit',
                bodyStyle: 'padding: 5px;',
                items: [{
                    xtype: 'tabpanel',
                    items: [{
                        title: 'Table Settings',
                        html: 'Hello world 1'
                    }, {
                        title: 'Indices',
                        html: 'Hello world 2'
                    }, {
                        title: 'Foreign Keys',
                        html: 'Hello world 2'
                    }, {
                        title: 'Triggers',
                        html: 'Hello world 3',
                        closable: true
                    },
                    {
                        title: 'Security',
                        html: 'Hello world 3',
                        closable: true
                    },]
                }]
            });
     
 
    //new database
    winSecurity = Ext.create('widget.window', {
                title: 'Security',
                closable: true,
                closeAction: 'hide',
                width: 600,
                minWidth: 350,
                height: 350,
                layout: 'border',
                bodyStyle: 'padding: 5px;',
                items: [{
                    region: 'west',
                    title: 'Navigation',
                    width: 200,
                    split: true,
                    collapsible: true,
                    floatable: false
                }, {
                    region: 'center',
                    xtype: 'tabpanel',
                    items: [{
                        title: 'Bogus Tab',
                        html: 'Hello world 1'
                    }, {
                        title: 'Another Tab',
                        html: 'Hello world 2'
                    }, {
                        title: 'Closable Tab',
                        html: 'Hello world 3',
                        closable: true
                    }]
                }]
            });  
    </script>

</head>
<body>
    <!-- use class="x-hide-display" to prevent a brief flicker of the content -->
    <div id="west" class="x-hide-display">
        <p>Hi. I'm the west panel.</p>
    </div>
    <div id="center2" class="x-hide-display">
        <textarea id="editwindow" rows="2" cols="20">
        If you're not seeing a code editor here there is a problem. 
        </textarea>
    </div>
    <div id="center1" class="x-hide-display">
        hey
        
    </div>
    <div id="props-panel" class="x-hide-display" style="width:200px;height:200px;overflow:hidden;">
    </div>
    <div id="south" class="x-hide-display">
        <p>south - generally for informational stuff, also could be for status bar</p>
    </div>
    
</body>
</html>
