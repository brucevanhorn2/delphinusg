Ext.require([
    'Ext.tab.*',
    'Ext.window.*',
    'Ext.tip.*',
    'Ext.layout.container.Border'
]);

Ext.onReady(function(){
    var win;
    
    var loginForm = Ext.create('Ext.form.Panel', {
                frame: true,
                region:'center',
                bodyPadding: 15,
                bodyBorder:false,
                border:0,
                fieldDefaults: {
                    labelAlign: 'left',
                    labelWidth: 100,
                    anchor: '100%'}
                    ,items: [
                             {
                            xtype: 'textfield',
                            name: 'username',
                            emptyText:'Use your MySQL username',
                            fieldLabel: 'User Name'
                        }
                        ,{
                            xtype: 'textfield',
                            emptyText:'Use your MySQL password',
                            name: 'password',
                            inputType:'password',
                            fieldLabel: 'Password'
                        }
                        ],fbar:[{
                            iconCls: 'help'
                            ,text:'Help'
                        },{
                            iconCls: 'login'
                            ,text: 'Login'
                            ,handler:function(){
                                
                                loginForm.submit({
                                    url:'php/authenticate.php'
                                    ,success:function(){
                                        window.location='delphinus.php';
                                    }
                                    ,failure:function(){
                                        Ext.MessageBox.show({
                                            title: 'Login Failed',
                                            iconCls:'errorDialogHeader',
                                            msg: 'Your username or password are incorrect.',
                                            buttons: Ext.MessageBox.OK
                                        });
                                    }
                                })
                                

                            }
                        }
                    ]
              
            });

        //if (!win) {
            win = Ext.create('widget.window', {
                title: 'Delphinvs Login',
                closable: false,
                width: 600,
                resizable:false,
                height: 250,
                layout: {
                    type: 'border',
                    padding: 5
                },
                items: [{
                        region:'west',
                        width:150,
                        bodyBorder:false,
                        border:0,
                        html:'<img src="img/dialogs/128/lock.png" />'
                    },loginForm,
                {
                    region:'south'
                    ,bodyBorder:false
                    ,border:0
                    ,html:'<p class="legal">Copyright 2012 Bruce Van Horn.  All rights reserved.  "Delphinvs" and the associated device / logo are subject to trademark restrictions in the United States of America and international law.  Delphinvs is distributed as open source software under a modified BSD license.  Please review the <a href="license.html">software license</a> prior to using the software.</p>'
                    ,padding:10
                }]
            });
        //}
        win.show();
    });
