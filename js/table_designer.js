/**
 * User: brucevanhorn
 * Date: 5/8/12
 * Time: 11:27 PM
 */

function setBlurListeners(){
    var table = document.getElementById("designer");
    var lastRow = table.rows.length;
    if(lastRow>1){
        //note that loops acting against the table will usually start with 1
        //because the top row has no fields in it
        for(var i=1; i<lastRow-1;i++){
            var fieldName = "row_" + i + "_comment_field";
            var commentField = document.getElementById(fieldName);
            commentField.onblur=null;
        }
    }

    var lastFieldName = "row_" + (lastRow-1) + "_comment_field";
    var lastCommentField = document.getElementById(lastFieldName);
    lastCommentField.onblur = onLastCommentFieldBlur;

}

//EVENT LISTENERS

function onDataTypeChange(evt){

    var incrementableTypes = new Array("int", "integer", "smallint", "bigint");
    var rowNumber = evt.target.id.substring(4,5);
    var dataType = evt.target.value;

    var autoIncFieldName = "row_" + rowNumber + "_auto_inc";
    var autoIncField = document.getElementById(autoIncFieldName);
    var idx = incrementableTypes.indexOf(dataType);

    if(idx > -1){
        autoIncField.disabled = false;
    } else {
        autoIncField.checked =false;
        autoIncField.disabled = true;
    }

    var sizeableTypes = new Array("varchar", "text", "blob");

    var columnSizeFieldName = "row_" + rowNumber + "_column_size";
    var columnSizeField = document.getElementById(columnSizeFieldName);

    idx = sizeableTypes.indexOf(dataType);

    if(idx > -1){
        columnSizeField.disabled = false;
    } else {
        columnSizeField.value = "";
        columnSizeField.disabled = true;
    }

}

function onPrimaryKeyChange(evt){
    //TODO:  support compound keys?  they're so bad!
    //at a minimum determine whether they picked a numeric and if so
    //automatically check auto increment.
    //also you can only have ONE auto increment so if one has already been
    //set up, this option should be disabled.
    console.log("Function onPrimaryKeyChange has not been implemented yet.");
}

function onNullableChange(evt){
    //TODO:  If its a primary key, it can't be nullable.
    console.log("Function onNullableChange has not been implemented yet.");
}

function onAutoIncrementCheck(evt){

    var rowNumber = evt.target.id.substring(4,5);
    var autoIncrement = evt.target.checked;
    var defaultValueFieldName = "row_" + rowNumber + "_default_value";
    var defaultValueField = document.getElementById(defaultValueFieldName);
    if(autoIncrement){
        defaultValueField.value = "";
        defaultValueField.disabled = true;
    } else {
        defaultValueField.disabled = false;
    }

}

function onColumnSizeBlur(evt){
    //TODO:  make sure its a number

}

function onLastCommentFieldBlur(evt){
    //check to make sure the column has a name
    //if so go ahead and add a new row

    var table = document.getElementById("designer");
    var lastRow = table.rows.length-1;

    var validationPass = true;
    var columnName = document.getElementById("row_" + lastRow + "_column_name").value;
    if(columnName.length < 1){
        validationPass = false;
        console.error("Column Name is not optional");
    }
    var columnType = document.getElementById("row_" + lastRow + "_data_type").value;
    if(columnType.selectedIndex < 1){
        validationPass = false;
        console.error("You must select a type!")
    }

    if(validationPass){
        addrow();
    }

}
//END EVENT LISTENERS

//GENERAL FUNCTIONS
function addrow(){
    //var table = $('#designer');            figure out why jquery isn't working!!!!
    var table = document.getElementById("designer");
    var lastRow = table.rows.length;
    var newRow = table.insertRow(lastRow);

    var columnNameCell = newRow.insertCell(0);
    var columnNameField = document.createElement('input');
    columnNameField.type="text";
    columnNameField.name="row_" + lastRow + "_column_name";
    columnNameField.id=columnNameField.name;

    columnNameCell.appendChild(columnNameField);

    var dataTypeCell = newRow.insertCell(1);
    var dataTypeField = document.createElement('select');
    dataTypeField.name = "row_" + lastRow + "_data_type";
    dataTypeField.id=dataTypeField.name;

    dataTypeField.options[0] = new Option("Select Data Type", "0");
    dataTypeField.options[1] = new Option("int", "int");
    dataTypeField.options[2] = new Option("integer", "integer");
    dataTypeField.options[3] = new Option("smallint", "smallint");
    dataTypeField.options[4] = new Option("mediumint", "mediumint");
    dataTypeField.options[5] = new Option("bigint", "bigint");
    dataTypeField.options[6] = new Option("decimal", "demical");
    dataTypeField.options[7] = new Option("float", "float");
    dataTypeField.options[8] = new Option("double", "double");
    dataTypeField.options[9] = new Option("bit", "bit");
    dataTypeField.options[10] = new Option("date", "date");
    dataTypeField.options[11] = new Option("datetime", "datetype");
    dataTypeField.options[12] = new Option("timestamp", "timestampe");
    dataTypeField.options[13] = new Option("year", "year");
    dataTypeField.options[14] = new Option("char", "char");
    dataTypeField.options[15] = new Option("varchar", "varchar");
    dataTypeField.options[16] = new Option("blob", "blob");
    dataTypeField.options[17] = new Option("text", "text");
    dataTypeField.options[18] = new Option("enum", "enum");
    dataTypeField.options[19] = new Option("set", "set");
    dataTypeField.onchange = onDataTypeChange;

    dataTypeCell.appendChild(dataTypeField);

    var autoIncCell = newRow.insertCell(2);
    var autoIncField = document.createElement('input');
    autoIncField.name = "row_" + lastRow + "_auto_inc";
    autoIncField.id = autoIncField.name;
    autoIncField.type="checkbox";
    autoIncField.onchange = onAutoIncrementCheck;

    autoIncCell.appendChild(autoIncField);

    var nullableCell = newRow.insertCell(3);
    var nullableField = document.createElement('input');
    nullableField.name = "row_" + lastRow + "_nullable";
    nullableField.id = nullableField.name;
    nullableField.type="checkbox";
    nullableField.onchange = onNullableChange;

    nullableCell.appendChild(nullableField);

    var primaryKeyCell = newRow.insertCell(4);
    var primaryKeyField = document.createElement('input');
    primaryKeyField.type = "checkbox";
    primaryKeyField.id = "row_" + lastRow + "_primaryKey";
    primaryKeyField.name = primaryKeyField.id;
    primaryKeyField.onchange = onPrimaryKeyChange;

    primaryKeyCell.appendChild(primaryKeyField);

    var columnSizeCell = newRow.insertCell(5);
    var columnSizeField = document.createElement('input');
    columnSizeField.type="text";
    columnSizeField.name="row_" + lastRow + "_column_size";
    columnSizeField.disabled = true;
    columnSizeField.id=columnSizeField.name;
    columnSizeField.onblur = onColumnSizeBlur;

    columnSizeCell.appendChild(columnSizeField);

    var defaultValueCell = newRow.insertCell(6);
    var defaultValueField = document.createElement('input');
    defaultValueField.type = "text";
    defaultValueField.id = "row_" + lastRow + "_default_value";
    defaultValueField.name = defaultValueField.id;

    defaultValueCell.appendChild(defaultValueField);

    var fieldCommentCell = newRow.insertCell(7);
    var fieldCommentField = document.createElement('input');
    fieldCommentField.id = "row_" + lastRow + "_comment_field";
    fieldCommentField.name = fieldCommentField.id;

    //something special here. We need to detect whether this field has focus so we can
    //determine exactly when to create a new row on the table.
    fieldCommentField.onblur = function(){this.focused=false;};
    fieldCommentField.onfocus = function(){this.focused=true;};

    fieldCommentCell.appendChild(fieldCommentField);

    var removeRowCell = newRow.insertCell(8);
    var removeRowButton = document.createElement('input');
    removeRowButton.type="button";
    removeRowButton.id="row_" + lastRow + "_remove_button";
    removeRowButton.value="Remove";
    removeRowButton.onclick=function(){removerow(lastRow)};
    removeRowCell.appendChild(removeRowButton);

    setBlurListeners();
    document.getElementById(columnNameField.id).focus();
}

function removerow(rownum){
    var designer = document.getElementById('designer');
    var rowcount = designer.rows.length;
    if (rownum<rowcount){
        designer.deleteRow(rownum);
    }
    setBlurListeners();
}

function createTable(){
    var designer = document.getElementById('designer');
    var rowcount = designer.rows.length;
    var tableName = document.getElementById('tablename').value;
    var schema = ""; //TODO:  make a schema selector
    var ddl = "create table " + tableName + "(";
    for(var i=0; i<rowcount; i++) {
        var row = designer.rows[i];
        var columnName = document.getElementById("row_" + i + "_column_name").value;
        var columnType = document.getElementById("row_" + i + "_data_type").value;
        var primaryKey = document.getElementById("row_" + i + "_primaryKey").checked;
        if(primaryKey){
            primaryKey = "PRIMARY KEY";
        } else {
            primaryKey = "";
        }
        var autoInc = document.getElementById("row_" + i + "_auto_inc").checked;
        if(autoInc){
            autoInc = "AUTO_INCREMENT";
        } else {
            autoInc = "";
        }
        var nullable = document.getElementById("row_" + i + "_nullable").checked;
        if(nullable){
            nullable = "NOT NULL";
        } else {
            nullable = "";
        }
        var size = document.getElementById("row_" + lastRow + "_column_size").value;

        var ddl_appender = columnName + " " + nullable + " " + autoInc + " " + primaryKey;
        if(i<rowcount){
            ddl_appender += ",";
        }

        ddl+=ddl_appender;
    }

    ddl=");";
    console.log(ddl); // for now
}

function loadTable(databasename, tablename){
    //TODO:  Implement Database Load (maybe not even here, maybe in PHP?)
}
