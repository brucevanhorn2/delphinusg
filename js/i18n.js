var i18n = {};

//branding
i18n.product = "Delphinvs";
i18n.tagline = "The Best Web UI for MySQL";
i18n.copyright = "Copyright 2012 Bruce Van Horn";


//common words & phrases
i18n.common.database = "Database";
i18n.common.welcome = "Welcome";
i18n.common.output = "Output";
i18n.common.properties = "Properties";
i18n.common.username = "User Name";
i18n.common.password ="Password";
i18n.common.login = "Login";

//sections on screens

//tooltips by screen