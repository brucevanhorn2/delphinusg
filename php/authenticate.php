<?php
session_start();
header("content-type:application/json");

include_once("config_reader.php");


    //authenticates by logging into mysql
    $userName = null;
    $password = null;
    
    if(isset($_POST["username"]) && isset($_POST["password"])){
        $userName = $_POST["username"];
        $password = $_POST["password"];
        
        $password = trim($password);
        $userName = trim($userName);
    } 
    
    $link = @mysql_connect('localhost:/tmp/mysql.sock', $userName, $password);
    if (!$link) {
        fail();
    } else {
        pass($userName, $password);
        mysql_close($link);
    }
    
    function pass($userName, $password){
        echo("{\"success\":true, \"error\":\"NONE\"}");
        $_SESSION["user.authenticated"] = true;
        $_SESSION["user.username"] = $userName;
        $_SESSION["user.password"] = $password;
        readConfig();
    }
    
    function fail(){
        $_SESSION["user.authenticated"] = false;
        die("{\"success\":false, \"error\":\"" . mysql_error() . "\"}");
    }

?>