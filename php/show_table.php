<?php

//pretty straightforward... get the records from a given table and display them in an editable grid.

//header("content-type:application/json");

$databaseName = $_GET["databasename"];
$tableName = $_GET["tablename"];
$itemsPerPage = $_GET["itemLimit"];
$startItem = $_GET["startItem"];

$link = mysql_connect('localhost:/tmp/mysql.sock', 'root', 'password');
if (!$link) {
    die('Could not connect: ' . mysql_error());
}

mysql_select_db($databaseName);
$sql = 'select * from ' . $tableName . ' limit ' . $itemsPerPage;
$result = mysql_query($sql);

$num_rows = mysql_num_rows($result);
$counter=0;

$num_cols = mysql_numfields($result);
$columnlist = mysql_query('show columns from ' . $tableName . ';');
$columns = array();

while($cols = mysql_fetch_array($columnlist)){
    array_push($columns, $cols[0]);
}
echo("[");

while($row = mysql_fetch_array($result)){
    echo("{");
    
    for($i=0;$i<$num_cols;$i++){
        echo("\"" . $columns[$i] . "\":\"" . $row[$i] . "\"");
        if($i<$num_cols-1){
            echo(", ");
        }
    }
    echo("}");
    if($counter<($num_rows-1)){
        echo(",");
    }
    $counter++;
}

echo("]");

mysql_close($link);

?>