<?php
session_start();

$username = $_SESSION["user.username"];
$password = $_SESSION["user.password"];

header("content-type:application/json");

$link = mysql_connect('localhost:/tmp/mysql.sock', $username, $password);
if (!$link) {
    die('Could not connect: ' . mysql_error());
}

if($_GET["node"] == "src"){
    getDatabases();    
} else {
    //its some other node
    //the id takes the format TYPE:value so split on :
    $parts = explode(":", $_GET["node"]);
    switch($parts[0]){
        case "DATABASE":
            getTableOptions($parts[1]);
            break;
        
        case "TABLE":
           getTables($parts[1]);
            break;
        case "FIELD":
           getFields($parts[1], $parts[2]);
            break;
        case "VIEW":
            getViews($parts[1]);
            break;
        case "PROCEDURE":
            getProcedures($parts[1]);
            break;
    }
}


function getDatabases(){
    $result = mysql_query('show databases');
    $num_rows = mysql_num_rows($result);
    $counter=0;
    
    echo("[");
    if (!$result) {
        die('Invalid query: ' . mysql_error());
    } else {
        while($row = mysql_fetch_array($result))
            {
                echo("{");
                echo("\"id\":\"DATABASE:" . $row['Database'] . "\"");
                echo(",\"text\":\"" . $row['Database'] . "\"");
                echo(",\"iconCls\":\"tree-database\"");
                echo(",\"leaf\":\"false\"");    
                echo("}");
                if($counter<($num_rows-1)){
                    echo(",");
                }
                $counter++;

            }

    }
    echo("]");

}

function getTableOptions($Database){
    
    echo("[");
        
        echo("{");
            echo("\"id\":\"TABLE:". $Database."\"");
            echo(",\"text\":\"Tables\"");
            echo(",\"iconCls\":\"tree-database\"");
            echo(",\"leaf\":\"false\""); 
        echo("}");
        
        echo(",{");
            echo("\"id\":\"VIEW:".$Database."\"");
            echo(",\"text\":\"Views\"");
           echo(",\"iconCls\":\"tree-database\"");
            echo(",\"leaf\":\"false\""); 
        echo("}");
        
        echo(",{");
            echo("\"id\":\"PROCEDURE:".$Database."\"");
            echo(",\"text\":\"Stored Procedures\"");
            echo(",\"iconCls\":\"tree-database\"");
            echo(",\"leaf\":\"false\""); 
        echo("}");
    echo("]");
}


function getTables($Database){
    mysql_select_db($Database);
    $result = mysql_query("show full tables where table_type = 'BASE TABLE'");
    $resultType = "FIELD";
    $num_rows = mysql_num_rows($result);
    $counter=0;
    echo("[");
    if (!$result) {
        die('Invalid query: ' . mysql_error());
    } else {
        while($row = mysql_fetch_array($result))
            {
                echo("{");
                echo("\"id\":\"" . $resultType . ":" . $Database .  ":" . $row[0] . "\"");
                echo(",\"text\":\"" . $row[0] . "\"");
                echo(",\"iconCls\":\"tree-database\"");
            echo(",\"leaf\":\"false\""); 
                echo("}");
                if($counter<($num_rows-1)){
                    echo(",");
                }
                $counter++;

            }

    }
    echo("]");
    cleanUp();
}

function getViews($Database){
    mysql_select_db($Database);
    $result = mysql_query("show full tables where table_type = 'VIEW'");
    $resultType = "FIELD";
    $num_rows = mysql_num_rows($result);
    $counter=0;
    echo("[");
    if (!$result) {
        die('Invalid query: ' . mysql_error());
    } else {
        while($row = mysql_fetch_array($result))
            {
                echo("{");
                echo("\"id\":\"" . $resultType . ":" . $Database .  ":" . $row[0] . "\"");
                echo(",\"text\":\"" . $row[0] . "\"");
                echo(",\"iconCls\":\"tree-database\"");
            echo(",\"leaf\":\"false\""); 
                echo("}");
                if($counter<($num_rows-1)){
                    echo(",");
                }
                $counter++;

            }

    }
    echo("]");
    cleanUp();
}


function getProcedures($Database){
    mysql_select_db($Database);
    $result = mysql_query("SELECT routine_name FROM information_schema.routines WHERE routine_type = 'PROCEDURE' and routine_schema LIKE '" . $Database . "';");
    $resultType = "FIELD";
    $num_rows = mysql_num_rows($result);
    $counter=0;
    echo("[");
    if (!$result) {
        die('Invalid query: ' . mysql_error());
    } else {
        while($row = mysql_fetch_array($result))
            {
                echo("{");
                echo("\"id\":\"" . $resultType . ":" . $Database .  ":" . $row[0] . "\"");
                echo(",\"text\":\"" . $row[0] . "\"");
                echo(",\"iconCls\":\"file\"");
            echo(",\"leaf\":\"true\""); 
                echo("}");
                if($counter<($num_rows-1)){
                    echo(",");
                }
                $counter++;

            }

    }
    echo("]");
    cleanUp();
}
function getFields($Database, $Table){
    mysql_select_db($Database);
    $result = mysql_query('show columns from ' . $Table . ';');
    $resultType = "FIELD";
    $num_rows = mysql_num_rows($result);
    $counter=0;
    echo("[");
    if (!$result) {
        die('Invalid query: ' . mysql_error());
    } else {
        while($row = mysql_fetch_array($result))
            {
                echo("{");
                echo("\"id\":\"" . $resultType . ":" . $row["Field"] . "\"");
                echo(",\"text\":\"" . $row["Field"] . " - " . $row["Type"] . "\"");
                echo(",\"iconCls\":\"tree-database\"");
            echo(",\"leaf\":\"true\"");
                echo("}");
                if($counter<($num_rows-1)){
                    echo(",");
                }
                $counter++;

            }

    }
    echo("]");
    cleanUp();
}
function resultToTreeJSON($resultType, $result){
    echo("[");
    if (!$result) {
        die('Invalid query: ' . mysql_error());
    } else {
        while($row = mysql_fetch_array($result))
            {
                echo("{");
                echo("\"id\":\"" . $resultType . ":" . $row['Database'] . "\"");
                echo(",\"text\":\"" . $row['Database'] . "\"");
                echo("}");
                if($counter<($num_rows-1)){
                    echo(",");
                }
                $counter++;

            }

    }
    echo("]");
    cleanUp();
}
function cleanUp(){
    //mysql_close($link);
}
?>