<?php

function readConfig(){
    //reads config file and sets session vars
    
    $ini_array = parse_ini_file("../conf/delphinus.ini", true);
    $database_settings = $ini_array["database"];
    
    $url = $database_settings["url"];
    $_SESSION["database.url"] = $url;
}
?>