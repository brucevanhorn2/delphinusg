<?php

    function generateInsert($database, $table){
        $ddl = "insert into " . $table."(";
        
        $link = mysql_connect('localhost:/tmp/mysql.sock', 'root', 'password');
        if (!$link) {
            die('Could not connect: ' . mysql_error());
        }
        
        $sql = "show columns from " . $table;
        $result = mysql_query($sql);
        $num_rows = mysql_num_rows($result);
        $counter = 0;
        while ($row = mysql_fetch_assoc($result)) {
            $ddl = $ddl . $row["Field"];
            if($counter<($num_rows-1)){
                $ddl = $ddl . ", ";
            }
            
            $counter++;
        }
        
        $ddl = $ddl . ") values (";
        
        $counter = 0;
        while ($row = mysql_fetch_assoc($result)) {
            $ddl = $ddl  . "<" . $row["Type"] . ">";
            if($counter<($num_rows-1)){
                $ddl = $ddl . ", ";
            }
           
            $counter++;
        }
        
         $ddl = $ddl . ");";
         return $ddl;
    }
    
    function generateUpdate($database, $table){
        $ddl = "update " . $table . " set ";
        
        $link = mysql_connect('localhost:/tmp/mysql.sock', 'root', 'password');
        if (!$link) {
            die('Could not connect: ' . mysql_error());
        }
        
        $sql = "show columns from " . $table;
        $result = mysql_query($sql);
        $num_rows = mysql_num_rows($result);
        $counter = 0;
        while ($row = mysql_fetch_assoc($result)) {
            $ddl = $ddl . $row["Field"] . " = <" . $row["Type"] . ">";
            if($counter<($num_rows-1)){
                $ddl = $ddl . ", ";
            }
            
            $counter++;
        }
        
         $ddl = $ddl . ");";
         return $ddl;
    }
    
    function generateTableDrop($table){
        //TODO:  deal with constraints
        $ddl = "drop table " .  $table;
        return $ddl;
    }
    
    function generateTableCreate($database, $table){
        
        $ddl = "create table " . $table . " (";
        
        $link = mysql_connect('localhost:/tmp/mysql.sock', 'root', 'password');
        if (!$link) {
            die('Could not connect: ' . mysql_error());
        }
        
        $sql = "show columns from " . $table;
        $result = mysql_query($sql);
        $num_rows = mysql_num_rows($result);
        $counter = 0;
        while ($row = mysql_fetch_assoc($result)) {
            $row["Field"] . " " . $row["Type"];
            
            if($row["Null"]=="NO"){
                $ddl = $ddl . " not null";
            }
            
            if($row["Extra"] == "auto_increment"){
                $ddl = $ddl . " auto_increment";    
            }
            
            if($row["Key"] == "PRI"){
                $ddl = $ddl . " primary key";    
            }
            
            if($counter<($num_rows-1)){
                $ddl = $ddl . ", ";
            }
            
            $counter++;
        }
        
        $ddl = $ddl . ");";
        return $ddl;
    
    //TODO:  indices
    //TODO: triggers
    //TODO: constraints
    
    }

?>