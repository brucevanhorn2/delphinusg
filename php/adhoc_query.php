<?php

header("content-type:application/json");

//warning.  runs whatever you throw at it.
//with great power comes great responsibilitah

$link = mysql_connect('localhost:/tmp/mysql.sock', 'root', 'password');
if (!$link) {
    die('Could not connect: ' . mysql_error());
}

$databasename = $_GET["databasename"];
$sql = urldecode(trim($_GET["sql"]));

mysql_select_db($databasename);

//figure out if its a select or not
$command = substr($sql,0,6);

//if its select then display the results of the select
echo("[");
if(trim(strtoupper($command)) == "SELECT"){
    
    $result = mysql_query($sql);
    
    $num_rows = mysql_num_rows($result);
    $counter=0;
    
    $num_cols = mysql_numfields($result);
        
    $columns = array();
    
    for($j=0;$j<$num_cols;$j++){
        array_push($columns, mysql_fieldname($result, $j));
    }
    
    
    while($row = mysql_fetch_array($result)){
        echo("{");
        
        for($i=0;$i<$num_cols;$i++){
            echo("\"" . $columns[$i] . "\":\"" . $row[$i] . "\"");
            if($i<$num_cols-1){
                echo(", ");
            }
        }
        echo("}");
        if($counter<($num_rows-1)){
            echo(",");
        }
        $counter++;
    }
    
    
} else {
    
    //its not a select so show the number of rows affected
    mysql_query($sql);
    echo("\"{rows_affected\":\"" . mysql_affected_rows($link) . "\"}");
    
}
echo("]");
if($link){
    mysql_close($link);
}
?>