<?php
/**
 * Created by JetBrains PhpStorm.
 * User: brucevanhorn
 * Date: 5/8/12
 * Time: 11:16 PM
 * To change this template use File | Settings | File Templates.
 */
    ///include_once('php/auth_header.php');
?>
<html>
<head>
    <title>Table Designer</title>
    <link rel="stylesheet" href="css/general.css" />
    <script language="javascript" type="text/javascript" src="../third-party/jquery1.72/jquery.js"></script>
    <script language="javascript" type="text/javascript" src="../js/table_designer.js"></script>
</head>
<body>

    <table id="tableinfo" border="1">
        <tr>
            <td>Schema</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>Table Name</td>
            <td><input type="text" id="tablename" name="tablename" /></td>
        </tr>
        <tr>
            <td>Storage Model</td>
            <td>
                <select id="storagemodel" name="storagemodel">
                    <option value="INNODB" selected="true">InnoDB</option>
                    <option value="MYISAM">MyISAM</option>
                    <option value="MEMORY">Memory</option>
                    <option value="CSV">CSV</option>
                    <option value="ARCHIVE">Archive</option>
                    <option value="BLACKHOLE">Black Hole</option>
                    <option value="FEDERATED">Federated</option>
                </select>
            </td>
        </tr>
    </table>
     <table id="designer" border="1">
         <tr>
             <td>Column Name</td>
             <td>Data Type</td>
             <td>Auto Inc</td>
             <td>Nullable</td>
             <td>Primary Key</td>
             <td>Size</td>
             <td>Default Value</td>
             <td>Field Description</td>
             <td>Remove</td>
         </tr>

     </table>
     <div id="toolbar">
         <input type="button" value="Create Table" onclick = "createTable();"/>
     </div>
<script language="javascript">
    addrow();
</script>
</body>
</html>